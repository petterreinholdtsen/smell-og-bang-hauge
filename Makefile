#
# Bruk pdflatex
# Fokusere på SLmaster-kjemi.tex?
# Endre alle JPEG til .jpeg
# Endre alle GIF til PNG og .png
# Hva er .mol?
# Bruk tresifret seksjonsnummerering på filene for riktig sortering
# Dropp chapter-filene

# Build book
PDFS = SLmaster-kjemi.pdf images/cover-art-nb.pdf

all: $(PDFS) fixmes

fixmes:
	@echo ============= Unfinished ================
	grep -a FIXME *.tex unfinished/*.tex| cut -d: -f2- | iconv -f latin1
	@echo =============== in PDF ==================
	grep -a FIXME *.tex exercise/*.tex| cut -d: -f2- | iconv -f latin1

hefte: all-groenn.tex hefte-groenn.pdf

clean:
	$(RM) *.aux *.idx *.log *.bak *~ *.maf *.mtc *.mtc0 *.toc *.blg *.bbl *.ind *.ilg *.bcf *.run.xml
	$(RM) exercise/*.aux unfinished/*.aux

distclean: clean
	$(RM) $(PDFS)

SLmaster-kjemi.pdf: Makefile SLmaster-kjemi.tex *.tex exercise/*.tex unfinished/*.tex *.bib
	pdflatex SLmaster-kjemi
	biber SLmaster-kjemi
	makeindex SLmaster-kjemi
	pdflatex SLmaster-kjemi
	pdflatex SLmaster-kjemi

hefte-groenn.pdf: Makefile hefte-groenn.tex all-groenn.tex exercise/*.tex *.bib
	pdflatex hefte-groenn
	biber hefte-groenn
	makeindex hefte-groenn
	pdflatex hefte-groenn
	pdflatex hefte-groenn

hefte-gul.pdf: Makefile hefte-gul.tex all-gul.tex exercise/*.tex *.bib
	pdflatex hefte-gul
	biber hefte-gul
	makeindex hefte-gul
	pdflatex hefte-gul
	pdflatex hefte-gul

hefte-ukjent.pdf: Makefile hefte-ukjent.tex all-ukjent.tex exercise/*.tex *.bib
	pdflatex hefte-ukjent
	biber hefte-ukjent
	makeindex hefte-ukjent
	pdflatex hefte-ukjent
	pdflatex hefte-ukjent

.gif.png:
	convert $^ $@

.tiff.png:
	convert $^ $@

.svg.pdf:
	inkscape -o $@ $^

.SUFFIXES: .tex .pdf .gif .png .tiff .svg

all-groenn.tex: exercise/*.tex
	for m in $$(grep -rl '% category: green' exercise/ |grep '.tex$$'|sed 's/.tex$$//'); do echo "\include{$$m}"; done > all-groenn.tex.new && mv all-groenn.tex.new all-groenn.tex

all-gul.tex: exercise/*.tex
	for m in $$(grep -rl '% category: yellow' exercise/ |grep '.tex$$'|sed 's/.tex$$//'); do echo "\include{$$m}"; done > all-gul.tex.new && mv all-gul.tex.new all-gul.tex

all-ukjent.tex: exercise/*.tex
	for m in $$(grep -rl '% category: unknown' exercise/ |grep '.tex$$'|sed 's/.tex$$//'); do echo "\include{$$m}"; done > all-ukjent.tex.new && mv all-ukjent.tex.new all-ukjent.tex
