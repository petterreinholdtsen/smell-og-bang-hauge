Ting å forbedre før utgivelse
=============================

* Omstrukturer filutlegg til å ha enkeltforsøk i underkatalog, en fil
  per forsøk. (gjort)

* Deler som må utbedres merkes med "FIXME".

* Det bør lages et vedlegg med tips til leverandører av utstyr og
  kjemikalier.

* Det bør være et avsnitt om destruksjon / avfallshåndtering til hvert
  forsøk.

* Hvert forsøk klassifiseres i skoletrinn, for å identifisere hvilke
  som er egnet for barneskoler (grønn), ungdomsskoler (gul),
  videregående og universitet (rød).

* Det bør noteres i exif-data i bildene hvem som er fotograf for de
  ulike fotografiene som brukes i boken, for eksempel
  images/finnsnes/forsidebilde.jpeg og images/host/host.jpeg.

* Hva er *.mol?  Kan leses av jmol.  Er det
  <URL: http://openbabel.org/docs/2.3.0/FileFormats/MDL_MOL_format.html >?
  <URL: https://tex.stackexchange.com/questions/52722/can-you-make-chemical-structure-diagrams-in-latex >


* Sjekk om alle forsøk fra
  <URL: https://www.forskerfabrikken.no/eksperimenter/ > er med i
  samlingen. listen med forsøk fra Forskerfabrikken sommerskole 2019
  (Glitrende krystaller og eksploderende ?):

   * Vulkan 1 (vulkanmodell)
   * Tenkeslim
   * Vulkanske eksplosjoner
   * Alunkrystaller
   * Eksploderende vann
   * Sprettball av latex
   * hvor mye vann kan en bleie inneholde
   * klistrete vann
   * sugerørsug
   * eksploderende såpebobler
