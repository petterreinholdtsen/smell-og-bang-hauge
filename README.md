Smell og bang!
==============

Manuskript for bok med oppskrifter på smell og bang-forelesninger

Lisensvilkårene for manuskriptet er [Creative Commons Navngivelse 4.0
Internasjonal (CC BY
4.0)](https://creativecommons.org/licenses/by/4.0/deed.no).

Autorativ versjon av manuskriptet er tilgjengelig fra
[Gitlab](https://gitlab.com/petterreinholdtsen/smell-og-bank-hauge).

Forsøkene er delt i tre kategorier, basert på hva som brukes og
potensielle konsekvenser ved problemer:

 - grønn, for småskolen
 - gul, for ungdomskolen
 - rød, for viderekommende (videregående og høgskoler)
